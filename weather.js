

const locationWord = document.querySelector("#w-location");
const countryWord = document.querySelector("#w-country");
const descWord = document.querySelector("#w-desc");
const tempWord = document.querySelector("#w-temp");

const maxTempWord = document.querySelector("#w-max-temp");
const minTempWord = document.querySelector("#w-min-temp");
const img = document.querySelector("#w-icon");


const humidity = document.querySelector("#w-humidity");
const pressure = document.querySelector("#w-pressure");
const windSpeed = document.querySelector("#w-wind");

const input = document.querySelector(".form-control");
const changeLocation = document.querySelector("#btn-btn");
const change = document.querySelector("#w-change-btn");


window.addEventListener("load", () => {
    navigator.geolocation.getCurrentPosition(position => {
        const lat = position.coords.latitude;
        const long = position.coords.longitude;
        getCountry(lat, long);
    })


    async function getCountry(lat, long) {
        let capital = await fetch(`https://geocode.xyz/${lat},${long}?geoit=json`);
        let coun = await capital.json();
        weather(coun.country);
    }
})


change.addEventListener("click", () => {
    if (input !== "") {
        let name = input.value;
        weather(name);
        input.value = "";
    }
})


async function weather(davlat) {
    let havo = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${davlat}&appid=2d6a242864886272d5ab939272c6d4b3&units=metric`);
    let res = await havo.json();
    console.log(res);
    locationWord.textContent = `City : ${res.name}`;
    countryWord.textContent = `Country : ${res.sys.country}`;
    descWord.textContent = `weather Condition : ${res.weather[0].description}`;
    tempWord.innerHTML = `Temperature : ${res.main.temp}` + "<sup> &#9900 </sup>";
    maxTempWord.textContent = `Max Temp : ${res.main.temp_max}`;
    minTempWord.textContent = `Max Temp : ${res.main.temp_min}`;
    humidity.textContent = `humidity : ${res.main.humidity} %`;
    pressure.innerHTML = `Air Pressure : ${res.main.pressure}` + "<sup> &#9900 </sup>";
    windSpeed.textContent = `Wind Speed : ${res.wind.speed} km/h`;
    img.src = `http://openweathermap.org/img/w/${res.weather[0].icon}.png`;
}   


